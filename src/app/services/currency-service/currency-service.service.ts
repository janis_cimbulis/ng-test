import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { CurrencyApiResponse } from '../../types';


@Injectable({
    providedIn: 'root'
})
export class CurrencyService {

    private apiURL = environment.currencyApiUrl;
    private symbols = '&symbols=';

    constructor(private httpClient: HttpClient) {
    }

    public getRates(currency?: string): Observable<CurrencyApiResponse> {
        const reqUrl = currency
            ? `${ this.apiURL }${ this.symbols }${ currency }`
            : this.apiURL;

        return this.httpClient.get<CurrencyApiResponse>(reqUrl);
    }
}
