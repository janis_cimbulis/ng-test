import { Component, OnDestroy, OnInit } from '@angular/core';
import { CurrencyService } from './services/currency-service/currency-service.service';
import { CurrencyApiResponse, CurrencyPointer, LoadStatus } from './types';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnInit, OnDestroy {
    title = 'NG-TEST';

    private currencySubscription: Subscription;
    private currencyChanged = new Subject<string>();

    private oldCurrencySymbol: string;
    public currencySymbol = 'EUR';
    public baseSymbol = 'USD';
    public amountFrom: number;

    public availableSymbols: Array<string>;

    public currencyFrom: CurrencyPointer;
    public currencyTo: CurrencyPointer;

    public loadStatus = LoadStatus.INITIAL;

    constructor(private currencyService: CurrencyService) {
    }

    get LoadStatusEnum() {
        return LoadStatus;
    }

    public selectCurrency(currency: string) {
        const updatable: CurrencyPointer = [ this.currencyTo, this.currencyFrom ]
            .find(cur => cur.pointsTo === this.oldCurrencySymbol);
        updatable.pointsTo = currency;
        this.oldCurrencySymbol = currency;

        this.currencyChanged.next();
    }

    public setAmountFrom(value: number) {
        this.amountFrom = value;
        this.currencyChanged.next();
    }

    private calculateRate() {
        this.loadStatus = LoadStatus.LOADING;
        this.currencyService
            .getRates(this.currencySymbol)
            .subscribe(((value: CurrencyApiResponse) => {
                const rate: number = Object.values(value.rates)[0];
                if (this.currencyFrom.pointsTo === this.currencySymbol) {
                    this.currencyTo.value = this.currencyFrom.value / rate;
                } else {
                    this.currencyTo.value = this.currencyFrom.value * rate;
                }
                this.loadStatus = LoadStatus.LOADED;
            }));
    }

    public swapToFrom() {
        const temp = this.currencyFrom;
        this.currencyFrom = { pointsTo: this.currencyTo.pointsTo, value: 0 };
        this.currencyTo = { pointsTo: temp.pointsTo, value: 0 };
    }

    ngOnInit(): void {

        // initial request to get available currencies
        this.currencyService
            .getRates()
            .subscribe((value: CurrencyApiResponse) => {
                this.availableSymbols = Object
                    .keys(value.rates)
                    .map(k => k);
            });

        this.currencyFrom = {
            pointsTo: this.baseSymbol,
            value: 0,
        };
        this.currencyTo = {
            pointsTo: this.currencySymbol,
            value: 0,
        };
        this.oldCurrencySymbol = this.currencySymbol;

        // defer user input
        this.currencySubscription = this.currencyChanged
            .pipe(debounceTime(500))
            .subscribe(() => this.calculateRate());
    }

    ngOnDestroy(): void {
        this.currencySubscription.unsubscribe();
        this.currencySubscription = null;
    }
}
