export interface CurrencyApiResponse {
    disclaimer: string;
    license: string;
    timestamp: number;
    base: string;
    rates: { [key: string]: number };
}

export interface CurrencyPointer {
    value: number;
    pointsTo: string;
}

export enum LoadStatus {
    INITIAL = 'INITIAL',
    LOADING = 'LOADING',
    ERROR = 'ERROR',
    LOADED = 'LOADED'
}
